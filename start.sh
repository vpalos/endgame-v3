#!/bin/bash -ue

#
# Configuration values are expected as environment variables.
# See `.env.example` for the required variables.
#

# Check configuration values.
if [ ${#BALANCER_ONION} -lt 62 ]; then
    echo "BALANCER_ONION variable must be a valid ?.onion URL!"
    exit 0
fi
if [ -z "$TOR_AUTH_PASSWORD" ]; then
    echo "TOR_AUTH_PASSWORD variable must be set!"
    exit 0
fi
if [ ${#KEY} -lt 68 ] || [ ${#KEY} -gt 128 ]; then
    echo "KEY variable must have between 68 and 128 characters!"
    exit 0
fi
if [ ${#SALT} -ne 8 ]; then
    echo "SALT variable must be exactly 8 characters long!"
    exit 0
fi

# Generate master key.
echo "Generating Master Key..."
SALT_HEX=$(echo -n "$SALT" | od -A n -t x1 | sed 's/ *//g')
export MASTER_KEY=$(openssl enc -aes-256-cbc -pbkdf2 -pass pass:$KEY -S $SALT_HEX -iter 2000000 -md sha256 -P | grep "key" | sed s/key=//g)
echo "Done. MASTER_KEY = $MASTER_KEY"

# Configure TOR.
if $ENABLE_TOR; then
    sed -i "s/#HiddenServiceOnionBalanceInstance/HiddenServiceOnionBalanceInstance/g" /etc/tor/torrc

    TOR_INTRO_DEFENSE_STATE=$([ "$TOR_INTRO_DEFENSE" = true ] && echo "1" || echo "0")
    sed -i "s/^HiddenServiceEnableIntroDoSDefense .*/HiddenServiceEnableIntroDoSDefense $TOR_INTRO_DEFENSE_STATE/g" /etc/tor/torrc

    TOR_POW_DEFENSE_STATE=$([ "$TOR_POW_DEFENSE" = true ] && echo "1" || echo "0")
    sed -i "s/^HiddenServicePoWDefensesEnabled .*/HiddenServicePoWDefensesEnabled $TOR_POW_DEFENSE_STATE/g" /etc/tor/torrc

    HASHED_TOR_AUTH_PASSWORD=$(tor --hash-password $TOR_AUTH_PASSWORD | tail -c 62)
    sed -i "s/HASHED_TOR_AUTH_PASSWORD/$HASHED_TOR_AUTH_PASSWORD/g" /etc/tor/torrc
fi

# Configure I2P.
if $ENABLE_I2P; then
    ENDGAME_DAT="/var/lib/i2pd/endgame.dat"
    while [ ! -f $ENDGAME_DAT ]; do
        sleep 1
    done
    export I2PHOSTNAME=$(head -c 391 $ENDGAME_DAT | sha256sum | cut -f1 -d\  | xxd -r -p | base32 | tr '[:upper:]' '[:lower:]' | sed -r 's/=//g').b32.i2p
fi

# Configure NGINX.
if ! $LOCAL_PROXY; then
    LOCAL_PROXY_URL="tor"
fi
sed -i "s/proxy_pass .*;/proxy_pass http:\/\/$LOCAL_PROXY_URL;/g" /etc/nginx/sites-enabled/site.conf

# Configure system values.
ulimit -n 65536
ulimit -c unlimited
export LD_LIBRARY_PATH=/usr/local/lib

# Background services.
service cron restart
service fail2ban restart

# Starts TOR and waits until it's bootstrapped.
start_tor_instance() {
    local tor_configuration_file="$1"
    
    # Start TOR and use process substitution to handle its output in real-time.
    coproc tor_process { tor -f "$tor_configuration_file" 2>&1; }
    
    # Monitor the output from the TOR process.
    while IFS= read -r line; do
        echo "$line"
        if [[ "$line" == *"Bootstrapped 100%"* ]]; then
            echo "TOR instance is ready."
            break
        fi
    done <&${tor_process[0]}

    # Close the file descriptor.
    exec {tor_process[0]}>&-
}

# Start everything.
if $ENABLE_TOR; then
    if [ -d "/opt/hidden_service" ]; then
        cp -r /opt/hidden_service /etc/tor/
    fi
    chown -Rc debian-tor:debian-tor /etc/tor
    chmod -Rc 0700 /etc/tor/hidden_service

    echo "MasterOnionAddress $BALANCER_ONION" >/etc/tor/hidden_service/ob_config

    start_tor_instance "/etc/tor/torrc"

    export TOR_HOSTNAME="$(cat /etc/tor/hidden_service/hostname)"
    echo "=============================================================================================="
    echo "EndGame address: $TOR_HOSTNAME"
    echo "=============================================================================================="
fi

if $ENABLE_I2P; then
    # TODO: This needs testing after Dockerization...
    i2pd &
fi

if ! $LOCAL_PROXY; then
    start_tor_instance "/etc/tor/torrc2" &
    start_tor_instance "/etc/tor/torrc3" &
    wait
    nohup socat UNIX-LISTEN:/run/tor_pass1.sock,fork,reuseaddr,unlink-early,user=www-data,group=www-data,mode=777 SOCKS4A:localhost:$BACKEND_ONION_1:80,socksport=9060 &
    nohup socat UNIX-LISTEN:/run/tor_pass2.sock,fork,reuseaddr,unlink-early,user=www-data,group=www-data,mode=777 SOCKS4A:localhost:$BACKEND_ONION_2:80,socksport=9070 &
fi

# Start Nginx.
/usr/local/openresty/nginx/sbin/nginx -c /etc/nginx/nginx.conf
