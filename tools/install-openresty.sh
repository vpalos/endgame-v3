#!/bin/bash -xue

# Get CPU count.
CPUS=$(grep -c processor /proc/cpuinfo)

# Get latest OpenResty source package.
RESTY_VERSION=$(curl -s https://api.github.com/repos/openresty/openresty/releases/latest | grep '"tag_name":' | sed -E 's/.*"v([^"]+)".*/\1/')
echo "Getting latest OpenResty package version $RESTY_VERSION..."
wget -q https://github.com/openresty/openresty/releases/download/v$RESTY_VERSION/openresty-$RESTY_VERSION.tar.gz
tar zxf openresty-$RESTY_VERSION.tar.gz

# NAXSI (WAF) NGINX module.
NAXSI_VERSION=$(curl -s https://api.github.com/repos/wargio/naxsi/releases/latest | grep "tag_name" | cut -d '"' -f 4)
echo "Getting NAXSI package version $NAXSI_VERSION..."
wget -q https://github.com/wargio/naxsi/releases/download/$NAXSI_VERSION/naxsi-$NAXSI_VERSION-src-with-deps.tar.gz
mkdir naxsi
tar zxf naxsi-$NAXSI_VERSION-src-with-deps.tar.gz -C naxsi

# Compile and install sources.
pushd openresty-$RESTY_VERSION
./configure -j$CPUS \
    --with-compat \
    --with-ipv6 \
    --with-pcre \
    --with-pcre-jit \
    --with-threads \
    --with-file-aio \
    --with-http_ssl_module \
    --with-stream_ssl_module \
    --with-stream_ssl_preread_module \
    --add-dynamic-module=../naxsi/naxsi_src
gmake -j$CPUS
gmake install
# Install OpenResty's LuaJit system-wide.
pushd build/LuaJIT-*
make install
popd # build/LuaJIT-*
popd # openresty-$RESTY_VERSION

# Add compiled modules.
git clone https://github.com/neoxic/lua-mongo.git
pushd lua-mongo
cmake -D USE_LUA_VERSION=jit .
make
make install
popd # lua-mongo

# Clean-up.
rm -rf openresty-$RESTY_VERSION.tar.gz
rm -rf openresty-$RESTY_VERSION
rm -rf naxsi-$NAXSI_VERSION-src-with-deps.tar.gz
rm -rf naxsi
rm -rf lua-mongo

# Nginx configuration.
cp -R /usr/local/openresty/nginx/conf /etc/nginx
