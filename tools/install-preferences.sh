#!/bin/bash -xue

# Enable TOR if chosen.
if $ENABLE_TOR; then
    sed -i 's/#torconfig//' /etc/nginx/sites-enabled/site.conf
fi

# Enable I2P if chosen.
if $ENABLE_I2P; then
    sed -i 's/#i2pconfig//' /etc/nginx/sites-enabled/site.conf
fi

# Front UI Styling Configuration.
sed -i "s/HEXCOLORDARK/$HEXCOLORDARK/g" /opt/front/queue.css
sed -i "s/HEXCOLOR/$HEXCOLOR/g" /opt/front/queue.css
sed -i "s|SQUARELOGO|$SQUARELOGO|" /opt/front/queue.css
sed -i "s|NETWORKLOGO|$NETWORKLOGO|" /opt/front/queue.css
sed -i "s/HEXCOLORDARK/$HEXCOLORDARK/g" /opt/front/queue.html
sed -i "s/HEXCOLOR/$HEXCOLOR/g" /opt/front/queue.html
sed -i "s/SITENAME/$SITENAME/g" /opt/front/queue.html
sed -i "s|FAVICON|$FAVICON|" /opt/front/queue.html
sed -i "s|SQUARELOGO|$SQUARELOGO|" /opt/front/queue.html
sed -i "s/SITENAME/$SITENAME/g" /opt/front/front.lua
sed -i "s|SITETAGLINE|$SITETAGLINE|" /opt/front/front.lua
sed -i "s/SITESINCE/$SITESINCE/g" /opt/front/front.lua
sed -i "s|FAVICON|$FAVICON|" /opt/front/front.lua
