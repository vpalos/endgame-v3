module gobalance

go 1.18

require (
	github.com/sirupsen/logrus v1.9.3
	github.com/stretchr/testify v1.8.0
	github.com/urfave/cli/v2 v2.25.7
	golang.org/x/crypto v0.13.0
	gopkg.in/yaml.v3 v3.0.1
	maze.io/x/crypto v0.0.0-20190131090603-9b94c9afe066
)

require (
	github.com/cpuguy83/go-md2man/v2 v2.0.2 // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/russross/blackfriday/v2 v2.1.0 // indirect
	github.com/xrash/smetrics v0.0.0-20201216005158-039620a65673 // indirect
	golang.org/x/sys v0.12.0 // indirect
)
