This folder contains Lua modules which are not bundled with OpenResty and
can't (shouldn't) be installed via OPM since they either have conflicting
dependencies or they simply clash with bundled modules.
