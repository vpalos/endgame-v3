local cook = require "resty.cookie"
local random = require "resty.random"
local tools = require "tools"
local front = require "front"
local ranks = require("ranks"):init()

-- For how long the captcha is valid. 120 sec is for testing, 3600 1 hour should be production.
local session_timeout = os.getenv("SESSION_LENGTH")

-- Determine allowed hosts based on service configuration.
local allowed_hosts = {}
if os.getenv("ENABLE_TOR") == "true" then
    table.insert(allowed_hosts, os.getenv("TOR_HOSTNAME"))
    table.insert(allowed_hosts, os.getenv("BALANCER_ONION"))
end
if os.getenv("ENABLE_I2P") == "true" then
    table.insert(allowed_hosts, os.getenv("I2P_HOSTNAME"))
end

local function ban_session(session_id)
    ranks:set(session_id, 1, 3600)
end

local function return_response(code, contentType, ...)
    ngx.header.content_type = contentType
    local content = { ... }
    for k, v in ipairs(content) do
        ngx.say(v)
    end
    ngx.flush()
    ngx.exit(code)
end

local function return_general_error()
    return_response(
        200,
        "text/plain",
        "403 DDOS filter killed your path. (You probably sent too many requests at once). Not calling you a bot, bot, but grab a new identity and try again."
    )
end

local function return_session_expired()
    return_response(
        401,
        "text/html",
        '<h1>EndGame Session has expired</h1> <h3>and the post request was not processed.</h3> <p><a target="_blank" href="/">After you pass another captcha</a> (clicking opens new tab), you can reload this page (press F5) and submit the request again to prevent data loss. <b>If you leave this page without submitting again, what you just submitted will be lost.</b></p>'
    )
end

local function return_ok()
    ngx.flush()
    ngx.exit(200)
end

local function kill_block_drop(proxy, session_id)
    if proxy ~= nil then
        tools.kill_connection(proxy)
    end
    if session_id ~= nil then
        ban_session(session_id)
    end
    ngx.exit(444)
end

-- Initialize cookie parser.
local cookie, err = cook:new()
if not cookie then
    ngx.log(ngx.ERR, err)
    return
end

-- Check proxy_protocol_addr if present kill circuit if needed.
local proxy_protocol_addr = "no_proxy"
if ngx.var.proxy_protocol_addr ~= nil then
    proxy_protocol_addr = ngx.var.proxy_protocol_addr
end

-- If "Host" header is invalid / missing, kill circuit and return nothing.
if tools.in_array(allowed_hosts, ngx.var.http_host) == nil then
    ngx.log(ngx.ERR, "Wrong host (" .. ngx.var.http_host .. ") " .. ngx.var.remote_addr .. "|" .. proxy_protocol_addr)
    kill_block_drop(proxy_protocol_addr, nil)
end

-- Only GET and POST requests are allowed the others are not used.
local requestMethod = ngx.var.request_method
if requestMethod ~= "POST" and requestMethod ~= "GET" then
    ngx.log(ngx.ERR, "Wrong request (" .. requestMethod .. ") " .. ngx.var.remote_addr .. "|" .. proxy_protocol_addr)
    kill_block_drop(proxy_protocol_addr, nil)
end

-- Requests without user-agent are usually invalid.
if ngx.var.http_user_agent == nil then
    ngx.log(ngx.ERR, "Missing user agent " .. ngx.var.remote_addr .. "|" .. proxy_protocol_addr)
    kill_block_drop(proxy_protocol_addr, nil)
end

-- POST without referer is invalid. some poorly configured clients may complain about this.
if requestMethod == "POST" and ngx.var.http_referer == nil then
    ngx.log(ngx.ERR, "Post without referer " .. ngx.var.remote_addr .. "|" .. proxy_protocol_addr)
    kill_block_drop(proxy_protocol_addr, nil)
end

-- Readn and validate cookie.
local session_id, err = cookie:get("dcap")
if not err and session_id ~= nil then
    if type(session_id) ~= "string" then
        ngx.log(ngx.ERR, "Invalid dcap value! Not string!" .. ngx.var.remote_addr .. "|" .. proxy_protocol_addr)
        kill_block_drop(proxy_protocol_addr, nil)
    end
    if not string.match(session_id, "^([A-Za-z0-9+/=]+)$") then
        ngx.log(ngx.ERR,
            "Invalid dcap value! Incorrect format! (" ..
            session_id .. ")" .. ngx.var.remote_addr .. "|" .. proxy_protocol_addr)
        kill_block_drop(proxy_protocol_addr, nil)
    end
end

-- Inform database of the new request in the context of its session ID.
if session_id then
    ranks:inform(session_id)
end

-- TODO!
-- The RANK is a spectrum (from 0 to 1) and eventually, we could trigger TOR's Proof-of-Work
-- to adjust a matching effort on the current session, but for now, we msut use it as boolean.
local is_banned = ranks:get(session_id) > 0.5
if is_banned then
    return_general_error()
end

-- Check dcap cookie get variable to bypass endgame! Allows some cross site attacks! Enable if need this feature.

-- local args = ngx.req.get_uri_args(2)
-- for key, val in pairs(args) do
--     if key == "dcapset" then
--         local plaintext = aes_256_gcm_sha256x1:decrypt(fromhex(val))
--         if not plaintext then
--             tools.kill_connection(proxy)
--             ban_session(session_id)
--             ngx.exit(444)
--         end
--         local cookdata = tools.split(plaintext, "|")
--         if (cookdata[1] == "captcha_solved") then
--             if (tonumber(cookdata[2]) + session_timeout) > ngx.now() then
--                 local ok, err =
--                 cookie:set(
--                 {
--                     key = "dcap",
--                     value = val,
--                     path = "/",
--                     domain = ngx.var.host,
--                     httponly = true,
--                     max_age = math.floor((tonumber(cookdata[2]) + session_timeout)-ngx.now()+0.5),
--                     samesite = "Lax"
--                })
--                if not ok then
--                    ngx.log(ngx.ERR, err)
--                    return
--                end
--                session_id = val
--                err = nil
--             end
--         end
--     end
-- end

front.set_error(nil)

-- check cookie support similar to testcookie
if requestMethod == "GET" then
    if err or session_id == nil then
        local ni = random.number(5, 20)
        local tstamp = ngx.now() + ni
        local plaintext = random.token(random.number(5, 20)) .. "|queue|" .. tstamp .. "|" .. proxy_protocol_addr .. "|"
        local ciphertext = tools.encrypt(plaintext)

        local ok, err =
            cookie:set(
                {
                    key = "dcap",
                    value = ciphertext,
                    path = "/",
                    domain = ngx.var.host,
                    httponly = true,
                    max_age = 30,
                    samesite = "Lax"
                }
            )
        if not ok then
            ngx.log(ngx.ERR, err)
            return
        end
        ngx.header["Refresh"] = ni
        ngx.header.content_type = "text/html"
        local queue = front.get_queue_html_file_content()
        ngx.say(queue)
        ngx.flush()
        ngx.exit(200)
    else
        local plaintext = tools.decrypt(session_id)
        if not plaintext then
            kill_block_drop(proxy_protocol_addr, session_id)
        end
        local cookdata = tools.split(plaintext, "|")
        if (cookdata[2] == "queue") then
            if tonumber(cookdata[3]) > ngx.now() or ngx.now() > tonumber(cookdata[3]) + 60 then
                kill_block_drop(proxy_protocol_addr, session_id)
            end

            --in high levels of attack this system may make reachability of your service worse. But it protects against certain kinds of dcap caching attacks.
            if "no_proxy" ~= cookdata[4] then
                if proxy_protocol_addr ~= cookdata[4] then
                    ngx.log(ngx.ERR, "QUEUE: Incorrect circuit id (" .. cookdata[4] .. ") for" .. proxy_protocol_addr)
                    kill_block_drop(proxy_protocol_addr, nil)
                end
            end

            -- captcha generator functions
            front.display_capd(proxy_protocol_addr)
            return_ok()
        elseif (cookdata[2] == "cap_not_solved") then
            if (tonumber(cookdata[3]) + 60) > ngx.now() then
                tools.kill_connection(proxy_protocol_addr)
                return_response(
                    200,
                    "text/html",
                    "<h1>THINK OF WHAT YOU HAVE DONE!</h1>",
                    "<p>That captcha was generated just for you. And look at what you did. Ignoring the captcha... not even giving an incorrect answer to his meaningless existence. You couldn't even give him false hope. Shame on you.</p>",
                    "<p>Don't immediately refresh for a new captcha! Try and fail. You must now wait about a minute for a new captcha to load.</p>"
                )
            end
            front.display_capd(proxy_protocol_addr)
            return_ok()
        elseif (cookdata[2] == "captcha_solved") then
            if (tonumber(cookdata[3]) + session_timeout) < ngx.now() then
                front.set_error("Session expired")
                front.display_capd(proxy_protocol_addr)
                return_ok()
            end
        else
            ngx.log(ngx.ERR,
                "No matching cook type data but valid parse! Encryption break? Cookie (" ..
                session_id .. ") [" .. plaintext .. "] circuit: " .. proxy_protocol_addr)
            kill_block_drop(proxy_protocol_addr, session_id)
        end
    end
end

if requestMethod == "POST" then
    --Will trigger under cookie loading error
    if err then
        return_session_expired()
    end

    if session_id ~= nil then
        local plaintext = tools.decrypt(session_id)
        if not plaintext then
            kill_block_drop(proxy_protocol_addr, session_id)
        end
        local cookdata = tools.split(plaintext, "|")
        if (cookdata[2] == "queue") then
            kill_block_drop(proxy_protocol_addr, session_id)
        elseif (cookdata[2] == "captcha_solved") then
            return
        elseif (cookdata[2] == "cap_not_solved") then
            if (tonumber(cookdata[3]) + session_timeout) < ngx.now() then
                front.set_error("Session expired")
                front.display_capd(proxy_protocol_addr)
                return_ok()
            end

            cookdata = tools.split(plaintext, "|")
            local expiretime = tonumber(cookdata[3])
            if expiretime == nil or (tonumber(expiretime) + 60) < ngx.now() then
                front.set_error("Captcha expired")
                front.display_capd(proxy_protocol_addr)
                return_ok()
            end

            -- resty has a library for parsing POST data but it's not really needed
            ngx.req.read_body()
            local dataraw = ngx.req.get_body_data()
            if dataraw == nil then
                front.set_error("You didn't submit anything. Try again.")
                front.display_capd(proxy_protocol_addr)
                return_ok()
            end

            local data = tools.split(dataraw, "&")
            local sentcap = ""
            local splitvalue = ""
            for index, value in ipairs(data) do
                if index > string.len(cookdata[5]) then
                    ngx.log(ngx.ERR,
                        "CAPTCHA SOLVE POST: EXCESSIVELY LONG ANSWER POST FOR ANSWER (" ..
                        cookdata[5] .. ") for" .. proxy_protocol_addr)
                    kill_block_drop(proxy_protocol_addr, session_id)
                    break
                end
                splitvalue = tools.split(value, "=")[2]
                if splitvalue == nil then
                    front.set_error("You Got That Wrong. Try again")
                    front.display_capd(proxy_protocol_addr)
                    return_ok()
                end
                sentcap = sentcap .. splitvalue
            end

            --in high levels of attack this system may make reachability of your service worse. But it protects against certain kinds of dcap caching attacks.
            if "no_proxy" ~= cookdata[4] then
                if proxy_protocol_addr ~= cookdata[4] then
                    ngx.log(ngx.ERR,
                        "CAPTCHA SOLVE POST: Incorrect circuit id (" .. cookdata[4] .. ") for" .. proxy_protocol_addr)
                    kill_block_drop(proxy_protocol_addr, session_id)
                end
            end

            if string.lower(sentcap) == string.lower(cookdata[5]) then
                --block valid sent cookies to prevent people from just sending the same solved solution over and over again
                ban_session(session_id)
                cookdata[1] = random.token(random.number(5, 20))
                cookdata[2] = "captcha_solved"
                cookdata[3] = ngx.now()
                cookdata[6] = "0"
                local ciphertext = tools.encrypt(table.concat(cookdata, "|"))
                local ok, err =
                    cookie:set(
                        {
                            key = "dcap",
                            value = ciphertext,
                            path = "/",
                            domain = ngx.var.host,
                            httponly = true,
                            max_age = session_timeout,
                            samesite = "Lax"
                        }
                    )
                if not ok then
                    ngx.say("cookie error")
                    return
                end
                local redirect_to = ngx.var.uri
                if ngx.var.query_string ~= nil then
                    redirect_to = redirect_to .. "?" .. ngx.var.query_string
                end
                return ngx.redirect(redirect_to)
            else
                front.set_error("You Got That Wrong. Try again")
            end
            front.display_capd(proxy_protocol_addr)
            return_ok()
        end
    else
        --Will trigger when cookie could be loaded but session_id isn't valid. Sanity check stuff.
        return_session_expired()
    end
end
