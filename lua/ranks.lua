-- The `rank` of a session is a floating-point number between 0 and 1,
-- where 0 means OK (benign traffic) and 1 means BAD (abusive traffic).

local mongo = require("mongo")
local redis = require("resty.redis")

local REDIS_HOST = os.getenv("REDIS_HOST")
local REDIS_PORT = os.getenv("REDIS_PORT")
local SESSION_LENGTH = tonumber(os.getenv("SESSION_LENGTH")) or 3600
local MONGODB_URI = os.getenv("MONGODB_URI")

local M = {}
M.__index = M

-- This ranking module will - by convention - select and use the database #0.
local RANK_DATABASE_ID = 0

-- Establish connections.
function M.init()
    local object = {}
    setmetatable(object, M)

    -- Redis connection.
    object.redis     = redis:new()
    local redis_host = REDIS_HOST
    local redis_port = REDIS_PORT
    local ok, err    = object.redis:connect(redis_host, redis_port) -- This recycles connections.
    if not ok then
        ngx.log(ngx.ERR, "Could not connect to Redis service " .. redis_host .. ":" .. redis_port .. ": " .. err)
        return ngx.exit(500)
    end
    object.redis:select(RANK_DATABASE_ID)

    -- MongoDB connection.
    local mongodb_client = mongo.Client(MONGODB_URI)
    object.mongodb = mongodb_client:getDatabase("endgame")

    return object
end

-- Explicitly set a RANK on the given session ID, in REDIS.
-- We also inform MongoDB of this RANK (since we don't yet have a better way to mark traffic).
function M:set(id, rank, seconds)
    -- We must have an expiration timeout.
    seconds = seconds or SESSION_LENGTH
    seconds = math.min(seconds, SESSION_LENGTH)

    -- Mark this session ID onto MongoDB.
    ngx.timer.at(0, function()
        -- Store in REDIS with expire timeout.
        self.redis:setex(id, seconds, rank)

        -- Store rank in MongoDB.
        local sessions = self.mongodb:getCollection("sessions")
        sessions:updateOne(
            { _id = id },
            { _id = id, rank = rank },
            { upsert = true }
        )
    end)
end

-- Get the current RANK for this given sessionID, from REDIS.
function M:get(id)
    local rank = self.redis:get(id)
    rank = tonumber(rank)
    if rank == nil then
        return 0
    end
    return rank
end

-- Informs MongoDB of a new request within the context of it's session ID and enqueues the
-- computation of its rank. The resulting rank will be available, once classified, in REDIS.
function M:inform(id)
    -- Compute new request.
    local payload = {
        session_id = id,
        request_uri = ngx.var.request_uri,
        user_agent = ngx.var.http_user_agent,
        scheme = ngx.var.scheme,
        host = ngx.var.host,
        query_string = ngx.var.query_string,
        request_body = ngx.var.request_body,
        time = ngx.var.time_iso8601
    }

    -- Store the request and the session ID onto MongoDB for analysis.
    ngx.timer.at(0, function()
        local sessions = self.mongodb:getCollection("sessions")
        local requests = self.mongodb:getCollection("requests")

        -- If the session is already ranked, there's nothing left to do.
        local session = sessions:findOne({ _id = id })
        if session ~= nil and session.rank ~= nil then
            return
        end

        -- Store session without rank (i.e. pending classification).
        if session == nil then
            sessions:insert({ _id = id })
        end

        -- Store new request on this session.
        requests:insert(payload)

        -- TODO QUEUE CLASSIFICATION
        -- Enqueue classification run on MindsDB.
    end)
end

return M
