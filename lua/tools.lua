local aes = require "resty.aes"
local hmac = require "resty.hmac"
local random = require "resty.random"
local sha256 = require "resty.sha256"

local M = {}

function M.hex2bin(hex_str)
    local bin_str = ""
    for i = 1, #hex_str, 2 do
        local hex_char = string.sub(hex_str, i, i + 1)
        bin_str = bin_str .. string.char(tonumber(hex_char, 16))
    end
    return bin_str
end

function M.hmac_digest(key, data)
    local hmac_sha256_lib = hmac:new(key, hmac.ALGOS.SHA256)
    if not hmac_sha256_lib then
        ngx.log(ngx.ERR, "Failed to initialize HMAC/SHA256!")
        return ngx.exit(500)
    end
    hmac_sha256_lib:update(data)
    return hmac_sha256_lib:final()
end

function M.sha256_digest(data)
    local sha256_lib = sha256:new()
    sha256_lib:update(data)
    return sha256_lib:final()
end

-- encryption key and salt must be shared across fronts. salt must be 8 chars
local key = os.getenv("KEY")
local salt = os.getenv("SALT")

-- generated in start.sh based on the encryption key using PBKDF2, which hardens it
-- against bruteforce attacks, making the implementation a little more foolproof, here's the command used:
-- OPENSSL 3:
-- openssl kdf -keylen 32 -kdfopt digest:SHA256 -kdfopt pass:$KEY -kdfopt salt:$SALT -kdfopt iter:2000000 PBKDF2 | sed s/://g
-- OPENSSL 1.1.1n:
-- openssl enc -aes-256-cbc -pbkdf2 -pass pass:$KEY -S $SALT_HEX -iter 2000000 -md sha256 -P | grep "key" | sed s/key=//g
local master_key = M.hex2bin(os.getenv("MASTER_KEY"))

-- This function encrypts the cookie and outputs it ready for use in the following format : base64(cookie_token + cookie_ciphertext + cookie_tag)
-- cookie_token is 32 bytes
-- cookie_ciphertext is variable
-- cookie_tag is 16 bytes
function M.encrypt(cookie_plaintext)
    local cookie_token = M.sha256_digest(random.token(32))
    local derived_key = M.hmac_digest(master_key, cookie_token)
    local aes_ctx = aes:new(derived_key, salt, aes.cipher(256, "gcm"), aes.hash.sha256, 1, 12)
    local encrypted = aes_ctx:encrypt(cookie_plaintext)
    return ngx.encode_base64(cookie_token .. encrypted[1] .. encrypted[2])
end

-- This function decrypts the cookie as it is received, no need to decode base64 or parse anything.
-- returns nil if any step of the decryption fails
function M.decrypt(cookie_ciphertext)
    local decoded_cookie = ngx.decode_base64(cookie_ciphertext)
    -- cookie should be at least 49 bytes long (32 for the token + 16 for the tag + at least 1 for the content)
    if (not decoded_cookie or #decoded_cookie <= 48) then
        return nil, "Decoded cookie empty or too short (<= 48 bytes)"
    end
    -- parsing the cookie
    local cookie_token = string.sub(decoded_cookie, 1, 32)
    local cookie_ciphertext = string.sub(decoded_cookie, 33, (#decoded_cookie - 16))
    local cookie_tag = string.sub(decoded_cookie, (#decoded_cookie - 15), #decoded_cookie)
    -- deriving the key and setting up AES context
    local derived_key = M.hmac_digest(master_key, cookie_token)
    local aes_ctx = aes:new(derived_key, salt, aes.cipher(256, "gcm"), aes.hash.sha256, 1, 12)
    return aes_ctx:decrypt(cookie_ciphertext, cookie_tag)
end

function M.in_array(tab, val)
    for index, value in ipairs(tab) do
        if value == val then
            return true
        end
    end
    return nil
end

function M.split(str, sep)
    local result = {}
    local index = 1
    local regex = ("([^%s]+)"):format(sep)
    for each in str:gmatch(regex) do
        result[index] = each
        index = index + 1
    end
    return result
end

function M.calc_circuit(proxyheaderip)
    if not proxyheaderip then
        return
    end
    local cg = M.split(proxyheaderip, ":")
    local g1 = cg[5]
    local g2 = cg[6]

    local glen = string.len(g1)
    if (glen < 4) then
        for i = (4 - glen), 1, -1 do
            g1 = "0" .. g1
            ::loop_label_1::
        end
    end
    glen = string.len(g2)
    if (glen < 4) then
        for i = (4 - glen), 1, -1 do
            g2 = "0" .. g2
            ::loop_label_2::
        end
    end

    local d1 = (string.sub(g1, 1, 1) .. string.sub(g1, 2, 2))
    local d2 = (string.sub(g1, 3, 3) .. string.sub(g1, 4, 4))
    local d3 = (string.sub(g2, 1, 1) .. string.sub(g2, 2, 2))
    local d4 = (string.sub(g2, 3, 3) .. string.sub(g2, 4, 4))
    local circuit_id = ((((bit.lshift(tonumber(d1, 16), 24)) + (bit.lshift(tonumber(d2, 16), 16))) + (bit.lshift(tonumber(d3, 16), 8))) + tonumber(d4, 16))
    return circuit_id
end

function M.kill_circuit(premature, clientip, headerip)
    local circuitid = M.calc_circuit(headerip)
    if not circuitid then
        return
    end
    local sockfile = "unix:/etc/tor/c1"
    local response = "Closing circuit " .. circuitid .. " "
    local sock = ngx.socket.tcp()
    sock:settimeout(1000)
    local ok, err = sock:connect(sockfile)
    if not ok then
        ngx.log(ngx.ERR, "failed to connect to tor: " .. err)
        return
    end
    ngx.log(ngx.ERR, "connected to tor")

    local tor_auth_password = os.getenv('TOR_AUTH_PASSWORD')
    local bytes, err = sock:send("authenticate \"" .. tor_auth_password .. "\"\n")
    if not bytes then
        ngx.log(ngx.ERR, "failed authenticate to tor: " .. err)
        return
    end
    local data, err, partial = sock:receive()
    if not data then
        ngx.log(ngx.ERR, "failed receive data from tor: " .. err)
        return
    end
    local response = response .. " " .. data

    local bytes, err = sock:send("closecircuit " .. circuitid .. "\n")
    if not bytes then
        ngx.log(ngx.ERR, "failed send data to tor: " .. err)
        return
    end
    local data, err, partial = sock:receive()
    if not data then
        ngx.log(ngx.ERR, "failed receive data from tor: " .. err)
        return
    end
    local response = response .. " " .. data

    ngx.log(ngx.ERR, response)
    sock:close()
end

function M.kill_connection(proxy)
    if proxy ~= "no_proxy" then
        local ok, err = ngx.timer.at(0, M.kill_circuit, ngx.var.remote_addr, ngx.var.proxy_protocol_addr)
        if not ok then
            ngx.log(ngx.ERR, "failed to create timer: ", err)
            return
        end
    end
end

return M
