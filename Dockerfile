FROM debian:bookworm

# Versions.
ENV DIST=debian
ENV RELEASE=bookworm

#
# BUILD-TIME CONFIGURATION START...
#

# Set to true if you want to use TOR.
ENV ENABLE_TOR=true

# Set to true if you want to use I2PD.
ENV ENABLE_I2P=false

# Enable Tor introduction defense.
# Keeps the Tor process from stalling but hurts reliability.
# Only recommended if running on low powered fronts.
ENV TOR_INTRO_DEFENSE=false

# Enable Tor POW introduction defense.
# This should be enabled!
ENV TOR_POW_DEFENSE=true

# CSS Branding.
ENV HEXCOLOR="9b59b6"
ENV HEXCOLORDARK="713C86"
ENV SITENAME="dread"
ENV SITETAGLINE="the frontpage of the dark net"
ENV SITESINCE="2018"
ENV FAVICON="data:image/x-icon;base64,AAABAAEAEBAAAAEAIABoBAAAFgAAACgAAAAQAAAAIAAAAAEAIAAAAAAAAAQAABMLAAATCwAAAAAAAAAAAACtRI7/rUSO/61Ejv+tRI7/rUSO/61Fjv+qPor/pzaG/6k7if+sQo3/qDiH/6g4h/+sQ43/rUSO/61Ejv+tRI7/rUSO/61Ejv+tRI7/rUSO/61Fjv+sQo3/uV6e/8iBs/+9aaT/sEyT/8V7r//Feq//sEqS/6xDjf+tRI7/rUSO/61Ejv+tRI7/rUSO/65Fj/+vR5D/rEGM/+fI3v///////fv8/+/a6f/+/f7/+vT4/7Zam/+rP4v/rkWP/61Ejv+tRI7/rUSO/61Fjv+sQYz/qTqI/6g4h//hudX/5sXc/+7Z6P////////7///ft9P+2WZr/q0CL/61Fj/+tRI7/rUSO/61Fj/+rQIv/uFyd/82Ou//Njrv/uWGf/6g6iP+uR5D/5sbc///////47vX/tlma/6s/i/+tRY//rUSO/61Ejv+uRo//qDqI/9aix///////69Hj/61Ejv+vSJD/qTqI/8BvqP//////+O/1/7ZZmv+rP4v/rUWP/61Ejv+tRI7/rkaP/6k8if/fttP//////9ekyP+oOIf/sEuS/6tAi/+7ZKH//vv9//nw9v+2WJr/qz+L/61Fj/+tRI7/rUSO/65Gj/+oOoj/1qHG///////pzeH/qj6K/6o8if+lMoP/0pjB///////47vX/tlma/6s/i/+tRY//rUSO/61Ejv+uRo//qj2K/7xmo//8+Pv//////+G61f+8ZqP/zpC8//v2+v//////+O/1/7ZZmv+rP4v/rUWP/61Ejv+tRI7/rUSO/65Gj/+pPIn/zo+7//79/v///////////////////v////////jw9v+2WZr/qz+L/61Fj/+tRI7/rUSO/61Ejv+tRI7/rUWP/6o9iv/Ab6j/37bT/+vR4//kwdr/16XI//36/P/58ff/tlma/6s/i/+tRY//rUSO/61Ejv+tRI7/rUSO/61Ejv+uRo//qj2K/6o9if+tRY7/qDmH/7VYmv/9+fv/+fH3/7ZYmv+rP4v/rUWP/61Ejv+tRI7/rUSO/61Ejv+tRI7/rUSO/65Gj/+uRo//rkaP/6s/i/+6Y6H//Pf6//ju9f+1WJr/q0CL/61Fj/+tRI7/rUSO/61Ejv+tRI7/rUSO/61Ejv+tRI7/rUSO/65Gj/+qPor/umOh//79/v/69Pj/tlqb/6s/i/+uRY//rUSO/61Ejv+tRI7/rUSO/61Ejv+tRI7/rUSO/61Ejv+tRI7/rEKN/7FNk//GfLD/xHmu/7BKkv+sQ43/rUSO/61Ejv+tRI7/rUSO/61Ejv+tRI7/rUSO/61Ejv+tRI7/rUSO/61Ejv+sQo3/qDiH/6g4h/+sQ43/rUSO/61Ejv+tRI7/AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=="
ENV SQUARELOGO="data:image/webp;base64,UklGRkwCAABXRUJQVlA4WAoAAAAQAAAATwAATwAAQUxQSC8AAAABH6CobRuIP9xev3ajERHxDWAqkg1PshdNnqeZJBfAR0T/J2ADngp0E/PbifAGPABWUDgg9gEAAFAMAJ0BKlAAUAA+bS6URiQioiEuGikggA2JQBoZQmgR/d8Cdib6gNsB5gPNy04Dej0a2NXoYw8XMJObQmvWHz52jkOgFebAO+W2C3pjVzORG7gs9ssx8qnjo3F96nSITr1LFsVBFyXhL7ywAP7cUf/iHNu+tvSh+rohhPqZvzMSRXtv7e8U/Dh5LwZIJHvcIx9GmDQsAejcJZBn+c5L63sC8fmQQAde+N776pYSR99TxW58l33OS2vwEbv0MLQQeKEkfOYxulikHr7tl/6ZwLnjENpEv6OnhDWVW53x32zxICdEDZ9TnGenzgbr1pGHJwZ3LX7o1h0RQkYVBag7IsYd+buU+m5wgCohMPbEwS+Vi02J7tVFAvUPVW5VfdjGFbzTfD5g/+nMoacT15PcUWxFNYCacgap7Zh5g0OaAa1rGJBuVbsFrGbbp5C85U3y+OuVTJcUt2wPNQJPA4lpjOyM5wGGRUxrjwy/+cOaOkfLlYc2ImOJLSmchU0u727olq8AMLRMZc/queUbr0E5ec/vKzZb9Z00D8dh5Hk6XNob6WDIFIusbW9UiKlnDFOz6ewFp4sONEBGGtI3gWG86cC+hmvHDzYlWupLMc2pRS9aO6FRiFAPmQXiJyK+lCZPtevTMxcUM4a6g3otDsiMGAOMbvIgAAA="
ENV NETWORKLOGO="data:image/webp;base64,UklGRpgFAABXRUJQVlA4WAoAAAAQAAAANgAAOwAAQUxQSCMFAAABoLBt2zE3HtddZhone8SubSt2bdu27a7rNqjtJlnvJqljJ7Xbqd1o0Ps4dt7v+6aTiIDgRpIiybnMu3cFb4DxaXs0+0hbmD4tbpO81cLkXLNJklmuptaLYnqa2kKJRaZVddJ9iQeTqppQRAKZK8glEyJMpdZeDdM6zhTM7phC7b66plB5/S1qFzqbFb5KYcqrQjOnOaW880P1bzYmi4xpDERz2VROWcoYoOFuMmfyt2n7z01m+KmAEP1d1TAON7urDwXMulyjLrmr8Vy2FjNjpB0AVTIXYCEXYj6v2QCwHaxmaZSnIisLAHaT1fz8swMAYBLzLLCcy2GRol0GALDf9ImvZtgDsLQSOUyJO9XHKaCAxfHNIMT9EfsBy7gMaDw5GGIaxxXzRqBTnzMJ0xwBuCaT1KVqXv8RDoh2MwrAAs4DKqlUlrbWlQEAYb+/0qTqSF5yQ41fKeRdf1SxEVI1sPRjHQvLauv4PTDq/Mlj5481h6WDjU0V9HlHIdvN/Z6KHsecSExKTExMunj4Lj+f/+vvcw+oDkY0STIM268lJiWdjH4kUgf0/kwxn3QUoiNZQjFzsfYryddtkCd8pE8Ufenrniyxwa1FSGhoWEDoQy4NCA4N9jvAs07YaPDGHx36h4a0cNsgcdkTAeI9VG9tAAAYW3IFQmZyGiTEF2ywVc23giAAkWkk74/N5ftYb6BJCTuI1nO9lOHjece+Y/bY+yRTIwEAAfr8ON0UxwU63p2NGEZD1HpRa2CD5MRs1V1yieMUfUK+PgCC+l8TPDOL66JuDHUF5QW1IZvVeoNOOETuq4d6xdk+CV/rS5oj0zGDF1RQtbtEFnkA3/XaGBMbtSsqemsmDdriwa0OKqjiOAvpbCFVaFOjgGMArODnXVXNRt4o+0pJJJ7FABjLAnPrAjkFLhjFO45o9FrbCxa7SVJBB2RqGsPpLsfAOV9BlctchyiuRvVYGsEPUYzCel6ppgxdy9UztU+8MY+C4luFahnBaPBaM1ut6QojYBvJGaj9WnCif22PTqufSgQBc0jugFFsiphcAxtpsBRC2tyRUSmRN62N9g/MrxschCQdiuXZwNgrjys3fEXySyNIc0xqLsntgFEPefHjf/AlyWvmMiZqZB4y65mmK4x6oQ0WIxBicNZSRv8vci+0zqgXGsW7jlAh3CBBzrBiuX8zxzsc/T/Ff+qxAOBfRvKOs4xVOvGf+mkMgDGSf2r5EopXwcbf1vsBSU6TUqVQdF8ooQuciXS2kC/Y+qhzhEOwPuPs6SvbbCWWayhXsPVRtzjLK15asIH6/Dj9NMf5WhaGQojNTz2rAnBfpiUV20N8nqQ9dE8n+XB8Dt/H+kBkls+dU8YuEopcvhnljnsgbUYh8q1PKl6u8pRan9jXA+B1Rb7RhgxuJfhdwWs/xUab7N73i1JbjwMAVbqCD10U2/rnXoFq0aNoYYhcX25QbXc55eOrOESe+FpsF73tK4wsRwsAgEXbrn6+XX39/f39fH0jQ2wUR9YvKrhdIqlL0bz+IwyS1J7YBEpRGpBJLgAcpyWc7uMYWMDiuKaiRq9e9gVG5Y8CeuSeMVcaxyfjJjsYO/yrzmGeBdZwDSxyuLqydPi/nGYPwMIKSnHbUcwXwqphJq4aizCfl8wlq0bJLncYnfb/aoxebLRJnU25Ri1hNNBwF5kzsaKWtgpaEStoITXp+vugQtZfAIsqZtkGelbUau+aJch0hanT4ibJG81h+rQ5kn2kDYwGAFZQOCBOAAAA0AQAnQEqNwA8AD5tLJJFpCKhmAQAQAbEtIAASD/dKxnoTlaUzRT/Q9kkRNlNuAAA/TH//9wDj/sFX/91UzfhNf+E8DE//4h3B/k1wAAA"

#
# ...BUILD-TIME CONFIGURATION END.
#

# Package repositories and keys.
ENV DEBIAN_FRONTEND noninteractive
RUN apt-get update && \
    apt-get install -y -q apt-transport-https ca-certificates curl wget gnupg2 debian-archive-keyring

# TOR repository & key.
# https://support.torproject.org/apt/
RUN wget -qO- https://deb.torproject.org/torproject.org/A3C4F0F979CAA22CDBA8F512EE8CBC9E886DDD89.asc | gpg --dearmor | tee /usr/share/keyrings/tor-archive-keyring.gpg >/dev/null
RUN echo "deb     [signed-by=/usr/share/keyrings/tor-archive-keyring.gpg] https://deb.torproject.org/torproject.org $RELEASE main" >/etc/apt/sources.list.d/tor.list
RUN echo "deb-src [signed-by=/usr/share/keyrings/tor-archive-keyring.gpg] https://deb.torproject.org/torproject.org $RELEASE main" >>/etc/apt/sources.list.d/tor.list
# For experimental (unstable) packages uncomment these (in addition!)...
# RUN echo "deb     [signed-by=/usr/share/keyrings/tor-archive-keyring.gpg] https://deb.torproject.org/torproject.org tor-experimental-$RELEASE main" >/etc/apt/sources.list.d/tor.list
# RUN echo "deb-src [signed-by=/usr/share/keyrings/tor-archive-keyring.gpg] https://deb.torproject.org/torproject.org tor-experimental-$RELEASE main" >>/etc/apt/sources.list.d/tor.list

# I2PD repository & key.
# https://i2pd.readthedocs.io/en/latest/user-guide/install/#debian
RUN wget -q -O - https://repo.i2pd.xyz/.help/add_repo | bash -s -

RUN apt-get update && apt-get install -y \
    build-essential zlib1g-dev libpcre3 libpcre3-dev uuid-dev \
    gcc git libpcre2-dev libpcre2-dev pkg-config \
    tor nyx socat deb.torproject.org-keyring i2pd \
    apt-listbugs needrestart debsecan debsums fail2ban \
    libpam-tmpdir rkhunter chkrootkit rng-tools \
    libssl3 libssl-dev cmake libmongoc-dev libbson-dev

# Setup fail2ban.
COPY ./etc/jail.local /etc/fail2ban/jail.local
RUN sed -i 's/#allowipv6 = .*/allowipv6 = auto/' /etc/fail2ban/fail2ban.conf

# Get sub-installers.
RUN mkdir /opt/endgame-v3
COPY ./tools /opt/endgame-v3/tools
WORKDIR /opt/endgame-v3/tools
RUN chmod -Rc +x *

# Install Nginx/Resty with required modules.
RUN ./install-openresty.sh
COPY ./lua-overrides /opt/lua-overrides

COPY ./etc/nginx.conf /etc/nginx/nginx.conf
COPY ./etc/naxsi_core.rules /etc/nginx/naxsi_core.rules
COPY ./etc/naxsi_whitelist.rules /etc/nginx/naxsi_whitelist.rules
RUN mkdir /etc/nginx/sites-enabled/
COPY ./etc/site.conf /etc/nginx/sites-enabled/site.conf

# System configurations.
RUN rm /etc/sysctl.conf
COPY ./etc/sysctl.conf /etc/sysctl.conf
COPY ./etc/limits.conf /etc/security/limits.conf

# These only get used selectively so it's OK to place them all now.
COPY ./etc/torrc /etc/tor/torrc
COPY ./etc/torrc2 /etc/tor/torrc2
COPY ./etc/torrc3 /etc/tor/torrc3
COPY ./etc/i2pd.conf /etc/i2pd/i2pd.conf
COPY ./etc/tunnels.conf /etc/i2pd/tunnels.conf

RUN mkdir /etc/nginx/cache/
RUN mkdir /var/log/nginx/
RUN chown -R www-data:www-data /etc/nginx/
RUN chown www-data:www-data /var/log/nginx

# Copy code and preferences.
COPY ./lua /opt/lua
COPY ./front /opt/front
RUN ./install-preferences.sh

# Cleanup.
RUN rm -rf /opt/endgame-v3/tools

# Configure log rotation configuration for nginx logs.
COPY ./etc/nginx.logrotate /etc/logrotate.d/nginx
RUN echo "0 0 * * * /usr/sbin/logrotate -f /etc/logrotate.conf" >/etc/cron.d/logrotate

# Regenerate Captcha every 5 minutes.
RUN echo "*/5 * * * * root cd /opt/front/ && ./captcha && nginx -s reload" >/etc/cron.d/endgame
RUN cd /opt/front/ && ./captcha

# Mountpoint for pre-set onion address.
VOLUME /opt/hidden_service

# Run EndGame.
WORKDIR /opt/endgame-v3
COPY ./start.sh /opt/endgame-v3/
CMD ./start.sh